/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sec.repository;


import com.sec.domain.Role;
import com.sec.domain.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
  User findByEmail(String email);
  
  User findByUsername(String username);
  
  User findById(Long id);
  
  //  User findByUsername(String username);

    @Override
    void delete(User user);

  
  
//  @Query(
//          value = "SELECT * FROM User u WHERE u.username = ?1",
//          nativeQuery = true)

  
    //List<User> findByCustomUsername(String username);
    
}
