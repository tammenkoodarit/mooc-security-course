package com.sec.repository;

import com.sec.domain.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public class UserRepositoryCustomImpl implements UserRepositoryCustom{

  @PersistenceContext
  EntityManager entityManager;
  
  @Override
  public List<User> findByCustomUsername(String username) {
    Query query = entityManager.createNativeQuery("SELECT * FROM User u WHERE u.username = '"+username+"'");
        //query.setParameter(1, username);
        return query.getResultList();
  }

}
