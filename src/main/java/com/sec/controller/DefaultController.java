package com.sec.controller;

import com.sec.domain.Role;
import com.sec.domain.User;
import com.sec.repository.RoleRepository;
import com.sec.repository.UserRepository;
import com.sec.repository.UserRepositoryCustom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DefaultController {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;
  
//    @Autowired
//    private UserRepositoryCustom userRepositoryCustom;
  
     @PersistenceContext
    EntityManager entityManager;
    
    @GetMapping("/")
    public String home1() {
        return "home";
    }

    @GetMapping("/home")
    public String home() {
        return "home";
    }
    
    @GetMapping("/registration/{role}")
    public String registration(Model model, @PathVariable String role) {
      if(role != null && !role.trim().isEmpty()){
        model.addAttribute("role",role.toUpperCase());
      }else{
        model.addAttribute("role","USER");
      }
        
        return "registration";
    }

    @PostMapping(value = "/registrationForm")
    public String registrationForm(Model model, @RequestParam(required = true) String username
      , @RequestParam(required = true) String password
      , @RequestParam(required = true) String firstname
    , @RequestParam(required = true) String lastname
    , @RequestParam(required = true) String email
    , @RequestParam(required = true) String role) {
        if(username != null && !username.trim().isEmpty()) {
          //Adding the new note always to the top.
          //this.listOfNotes.add(0,note);
          User findByUsername = userRepository.findByUsername(username);
          if(findByUsername != null){
            model.addAttribute("user_not_created","Username "+username+" exist, try other one");
            return "registration";
          }
            
           
           User user = new User();
           user.setFirstname(firstname);
           user.setLastname(lastname);
           user.setUsername(username);
           user.setPassword(password);
           user.setEmail(email);
           user.setEnabled(true);
           if(role != null && !role.trim().isEmpty()){
             Role userRole = roleRepository.findByName(role);
             user.setRoles(Arrays.asList(userRole));
           }else{
             Role userRole = roleRepository.findByName("USER");
             user.setRoles(Arrays.asList(userRole));
           }
           userRepository.save(user);
        }

        model.addAttribute("user_created","Registration successfull for username: "+username);
        return "home";
    }
    
    
    
    @GetMapping("/admin")
    public String admin() {
        return "admin";
    }

    
    @GetMapping("/user")
    public String user(Model model) {
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Query query = entityManager.createQuery("SELECT user FROM User user WHERE user.username = '"+username+"'");
        model.addAttribute("users", query.getResultList());
        return "user";
    }
    
    @GetMapping("/user/edit/{id}")
    public String userEdit(Model model,@PathVariable Long id) {
        User user = userRepository.findById(id);
        model.addAttribute(user);
        return "user/edit";
    }
    
    @PostMapping("/user/edit/{id}")
  public String userEditAndSave(Model model,
           @PathVariable Long id,
           @RequestParam(required = false) String username,
           @RequestParam(required = true) String password,
           @RequestParam(required = true) String firstname,
           @RequestParam(required = true) String lastname,
           @RequestParam(required = true) String email,
           @RequestParam(required = false) String address,
           @RequestParam(required = false) String postalCode,
  @RequestParam(required = false) String city,
  @RequestParam(required = false) String creditcardnumber) {
    User user = userRepository.findById(id);
    user.setUsername(username);  
    user.setFirstname(firstname);
      user.setLastname(lastname);
      user.setPassword(password);
      user.setEmail(email);
      user.setAddress(address);
      user.setPostalCode(postalCode);
      user.setCreditcardnumber(creditcardnumber);
      user.setCity(city);
      
      //user.setEnabled(true);
    userRepository.save(user);
    model.addAttribute(user);
    
    boolean hasAdminRole = false;
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
      for (GrantedAuthority authority : authorities) {
      String authority1 = authority.getAuthority();
        if(authority1.equals("ADMIN")){
          hasAdminRole = true;
          break;
        }
      }
    
    if(hasAdminRole){
      return "redirect:/admin/users";
    }else{
      return "redirect:/user";
    }
  }
    

    @GetMapping("/about")
    public String about() {
        return "about";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/403")
    public String error403() {
        return "error/403";
    }
    
    //EXAMPLE admin' OR 1=1 OR user.username='
    @GetMapping(value = "admin/users")
    public String users(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Query query = entityManager.createQuery("SELECT user FROM User user WHERE user.username = '"+username+"'");
//        Query query = entityManager.createQuery("SELECT user FROM User user WHERE user.username = :username");
//        query.setParameter("username", username);
        
        model.addAttribute("users", query.getResultList());
        
        return "admin/users";
    }
    
    @PostMapping(value = "admin/users")
    public String usersSearch(Model model, @RequestParam(required = true) String username) {
        //String username = "admin' OR 1=1 OR user.username='";
        if(username == null || username.isEmpty()){
          Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
          username = authentication.getName();
        }
        Query query = entityManager.createQuery("SELECT user FROM User user WHERE user.username LIKE '%"+username+"%'");
        model.addAttribute("users", query.getResultList());
        
//        Query query = entityManager.createQuery("SELECT user FROM User user WHERE user.username LIKE  CONCAT('%',:username,'%')");
//        query.setParameter("username", username);
        model.addAttribute("users", query.getResultList());
        
        return "admin/users";
    }
    

}
