insert into role values (10,'USER');
insert into role values (20,'ADMIN');

insert into user (id,firstname,lastname,email,username,password,enabled, creditcardnumber) values (1,'Adam','AdminUser','admin@securitytest.com','admin','1q2w3e4r',true, '4221460180909773');
insert into user (id,firstname,lastname,email,username,password,enabled, creditcardnumber) values (2,'User','UserUser','user@securitytest.com','user','1q2w3e4r',true, '4024007181609818');
insert into user (id,firstname,lastname,email,username,password,enabled, creditcardnumber, address, postalcode, city) values (3,'Ian','Normal','ian@securitytest.com','ian','password',true, '4485889661322141', 'Test street','00100','Test');
insert into user (id,firstname,lastname,email,username,password,enabled) values (4,'Jerry','Abnormal','jerry@securitytest.com','jerry','password12345',true);

insert into users_roles values (1,20);
insert into users_roles values (2,10); 
insert into users_roles values (3,10); 
insert into users_roles values (4,10); 