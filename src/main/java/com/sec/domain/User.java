package com.sec.domain;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class User {

    public User() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private String username;
    private String password;
    private boolean enabled;
    @Column(name = "creditcardnumber")
    private String creditcardnumber;
    @Column(name = "address")
    private String address;
    @Column(name = "postalcode")
    private String postalCode;
    @Column(name = "city")
    private String city;
 
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable( 
        name = "users_roles", 
        joinColumns = @JoinColumn(
          name = "user_id", referencedColumnName = "id"), 
        inverseJoinColumns = @JoinColumn(
          name = "role_id", referencedColumnName = "id")) 
    private Collection<Role> roles;

    public void setId(Long id) {
        this.id = id;
    }
    
    public void setFirstname(String firstName) {
        this.firstname = firstName;
    }
    
    public void setLastname(String lastName) {
        this.lastname = lastName;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
      this.username = username;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
  
    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstname() {
        return firstname;
    }

    public Long getId() {
        return id;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPassword() {
        return password;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public boolean isEnabled() {
        return enabled;
    }



  public String getUsername() {
    return username;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void setCreditcardnumber(String creditcardnumber) {
    this.creditcardnumber = creditcardnumber;
  }

  public String getCreditcardnumber() {
    return creditcardnumber;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getAddress() {
    return address;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public String getCity() {
    return city;
  }


  
    
    
}
