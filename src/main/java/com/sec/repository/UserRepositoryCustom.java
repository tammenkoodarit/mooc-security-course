package com.sec.repository;

import com.sec.domain.User;
import java.util.List;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserRepositoryCustom {

  List<User> findByCustomUsername(String username);
  
}
